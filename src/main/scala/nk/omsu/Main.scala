package nk.omsu

import java.io._

import scala.io.Source
import scala.util.Sorting

/**
  * Created by nk16 on 06.09.16.
  */

object Main {
  def gcd (a:Int, b:Int):Int =
    if (b == 0)
      a
    else
      gcd (b, a % b)
  def extended_euclid( a: Int,  b:Int):Int ={
    var  q, r, x1, x2, y1, y2, x, d, y: Int = 0
    var a2 = a
    var b2 = b
    if (b == 0) {
      d = a2
      x = 1
      y = 0
    }

    x2 = 1; x1 = 0; y2 = 0; y1 = 1

    while (b2 > 0) {
      q = a2 / b2
      r = a2 - q * b2
      x = x2 - q * x1
      y = y2 - q * y1
      a2 = b2
      b2 = r
      x2 = x1; x1 = x; y2 = y1; y1 = y
    }

    x2
  }
  def readBytes(file:File):Array[Byte] = {

    val is = new FileInputStream(file)
    val len = file.length.toInt
    val bytesInp = new Array[Byte](len)
    var offset = 0
    var numRead = is.read(bytesInp, offset, len - offset)

    while(offset < len && numRead == 0) {
      offset += numRead
      numRead = is.read(bytesInp, offset, len - offset)
    }
    is.close()
    bytesInp
  }
  def printStr(fileName: String, array: Array[String]): Unit ={
    val out = new PrintWriter(fileName)
    var s = "z"
    for (i <- array.indices){
      if (array(i) == null){
       array(i) = s
      }
    }
    Sorting.quickSort(array)
    for (str <- array){

        if ((str.indexOf('и') != -1 || str.indexOf('е') != -1 || str.indexOf('а') != -1) &&(str != null) && (str(0) != 'й') && (str(0) != 'ь') && (str(0) != 'ы') && (str(0) != 'э') && (str(0) != 'ю') && (str(0) != 'я') && (str(0) != 'ъ')) {
          out.println()
          out.println(str)
        }
      }

    out.close()
  }
  /*def encryption(a: Long, b: Long, m: Long, fileNameE:String):Unit = {

    val bytesInp = Source.fromFile(fileNameE, "UTF-8").mkString
    var bytesOut = new String

    for (i <- bytesInp.indices) {
      var M:Int = bytesInp(i)
      var E = a * M + b % m
      bytesOut += E.toByte
    }
    System.out.println("EncrypteBytes: " + bytesOut)
    printStr(fileNameE, bytesOut)
  }*/
  def decryption(a: Int, b: Int, m: Int, fileNameD:String):String ={
   // val bytesInp2 = readBytes(new File(fileNameD))
    // ъиъщящснэршисвнутсхреопсзовднврвфщсшиычдщядбоыишидрбнспирщртяыиэчещяещиэсзоэшсъощдслидргожишид
    val tab = Array('а','б','в','г','к','е','ж','з','и','й','д','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ь','ы','ъ','э','ю','я')
    val bytesInp = Source.fromFile(fileNameD, "UTF-8").mkString
    var bytesOut = new String
    var massIndex = new Array[Int](bytesInp.length)
   // println()
    for (i <- bytesInp.indices){
      for (j <- tab.indices){
        if(bytesInp(i) == tab(j)){
          massIndex(i) = j
     //     print(massIndex(i))
        }
      }
    }
    //println()
    //println()

      for (i <- bytesInp.indices){
        var C = massIndex(i)
        var s: Int = 0
        if (gcd(a, m) == 1)
          s = extended_euclid(a, m)
        if(s < 0)
          s = m + s
        var D:Int =  (s*(C-b)) % m
        if (D < 0)
          D = m + D
        bytesOut += tab(D)
      }
   // println()
     /* if(bytesOut(0) == tab(4)){
        println()
        println("A = " + a, "B = " + b)
        println()
      }*/

    //printStr(filenameDD, bytesOut)

    bytesOut
  }
  def main(args:Array[String]) :Unit ={
   // var a: Int = 5
    //var b: Int = 8
    var m: Int = 32
    val filenameE = "/home/nk16/Dropbox/1 семестр/криптографические методы защиты информации/LABS/Lab3(scala, sbt, KMZ)/src/main/resources/f.txt"
    val filenameDD = "/home/nk16/Dropbox/1 семестр/криптографические методы защиты информации/LABS/Lab3(scala, sbt, KMZ)/src/main/resources/d.txt"
    var stringS:Array[String] = new Array[String](520)
   // encryption(a, b, m, filenameE)
    var j = 0
    for (a <- 1 until (32, 2) ){
      for (b <- 0 until 32){
        stringS(j) = decryption(a, b, m, filenameE)
        if(a == 21 && b == 8){
          println()
          println(stringS(j))
          println()
          println("A = " + a, "B = " + b)
        }
        j += 1
      }
    }
    printStr(filenameDD, stringS)

  }
}